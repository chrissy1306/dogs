<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('breed');
            $table->string('url')->unique();
            $table->text('description');
            $table->timestamps();
        });

        DB::table('dogs')->insert([
            [ 'breed' => 'Beagle', 'url' => 'beagle.jpeg', 'description' => 'The beagle is a breed of small hound that is similar in appearance to the much larger foxhound.'],
            [ 'breed' => 'Bulldog', 'url' => 'bulldog.jpeg', 'description' => 'The Bulldog, also known as the British Bulldog or English Bulldog, is a medium-sized breed of dog.'],
            [ 'breed' => 'Basenji', 'url' => 'basenji.jpeg', 'description' => 'The Basenji is a breed of hunting dog. It was bred from stock that originated in central Africa.'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dogs');
    }
}
