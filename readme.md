# Dog App

Example of laravel application that shows list of dog breeds, click on individual dog show breed information.

## TO RUN

Need to have php 7.2 
 
```
php artisan serve
```
