@extends('layouts.app')

@section('content')
  <div class="panel panel-default">
    <h1>{{ $dog->breed }}</h1>
    <p>{{ $dog->description }}</p>
    <img src="{{url('/images/'.$dog->url)}}" alt="{{$dog->breed}}"/>
  </div>
@endsection