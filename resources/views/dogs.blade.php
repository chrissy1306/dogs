@extends('layouts.app')

@section('content')
  <!-- List of dogs -->
  @if (count($dogs) > 0)
    <div>
        <h1>Dogs</h1>
        <ul class="flex">
            @foreach ($dogs as $dog)
              <li class="dog">
                <a href="{{ URL('/dogs/'.$dog->id) }}">
                  <img src="{{url('/images/'.$dog->url)}}" alt="{{$dog->breed}}"/>
                  <p>{{ $dog->breed }}</p>
                </a>
              </li>
            @endforeach
        </ul>
    </div>
  @endif

@endsection