<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Dogs</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            nav {
                display: flex;
                justify-content: flex-end;
            }

            ul {
                list-style-type: none;
                padding: 0;
            }

            a {
                text-decoration: none;
            }

            img {
                height: 200px;
                width: 200px;
            }

            .full-height {
                height: 100vh;
            }

            .container {
                padding: 32px;
            }

            .flex {
                display: flex;
            }

            .dog {
                margin: auto;
            }

        </style>
    </head>
    <body>
        <div class="container">        
            <nav>
                <a href="/">home</a>
            </nav>
            <div>
               @yield('content')
            </div>
        </div>
    </body>
</html>
