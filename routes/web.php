<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// display all dogs

Route::get('/', function () {
  $dogs = \App\Dog::all();
  return view('dogs', [
    'dogs' => $dogs
  ]);
});

// display specific dog

Route::get('/dogs/{id}', function ($id) {
  $dog = \App\Dog::find($id);
  return view('dog', [
    'dog' => $dog
  ]);
});